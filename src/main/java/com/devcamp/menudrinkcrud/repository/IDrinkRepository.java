package com.devcamp.menudrinkcrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.menudrinkcrud.model.Drink;

public interface IDrinkRepository extends JpaRepository<Drink, Long> {

}
