package com.devcamp.menudrinkcrud.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.menudrinkcrud.model.Menu;
import com.devcamp.menudrinkcrud.repository.IMenuRepository;

@CrossOrigin
@RestController
@RequestMapping("/menu/")
public class MenuController {
    @Autowired
    IMenuRepository pMenuRepository;

    @GetMapping("/all")
    public ResponseEntity<List<Menu>> getAllMenus() {
        try {
            List<Menu> pMenu = new ArrayList<Menu>();
            pMenuRepository.findAll().forEach(pMenu::add);
            return new ResponseEntity<>(pMenu, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/detail/{id}")
    public ResponseEntity<Menu> getMenuById(@PathVariable("id") long id) {
        try {
            Optional<Menu> menuData = pMenuRepository.findById(id);
            if (menuData.isPresent()) {
                return new ResponseEntity<>(menuData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Menu> createMenu(@RequestBody Menu pMenus) {
        try {
            pMenus.setSize(pMenus.getSize());
            pMenus.setDuongKinh(pMenus.getDuongKinh());
            pMenus.setSuon(pMenus.getSuon());
            pMenus.setSalad(pMenus.getSalad());
            pMenus.setSoLuongNuocNgot(pMenus.getSoLuongNuocNgot());
            pMenus.setDonGia(pMenus.getDonGia());
            Menu _menus = pMenuRepository.save(pMenus);
            return new ResponseEntity<>(_menus, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Menu> updateMenu(@PathVariable("id") long id, @RequestBody Menu pMenus) {
        try {
            Optional<Menu> menuData = pMenuRepository.findById(id);
            if (menuData.isPresent()) {
                Menu menu = menuData.get();
                menu.setSize(pMenus.getSize());
                menu.setDuongKinh(pMenus.getDuongKinh());
                menu.setSuon(pMenus.getSuon());
                menu.setSalad(pMenus.getSalad());
                menu.setSoLuongNuocNgot(pMenus.getSoLuongNuocNgot());
                menu.setDonGia(pMenus.getDonGia());
                return new ResponseEntity<>(pMenuRepository.save(menu), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Menu> deleteMenu(@PathVariable("id") long id) {
        try {
            pMenuRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
